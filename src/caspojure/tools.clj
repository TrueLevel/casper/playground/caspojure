(ns caspojure.tools
  (:require [clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.spec.alpha :as s]
            [com.walmartlabs.datascope :as ds]
            [rhizome.viz :as viz]))

(defn dot
  "Given a root composite value, returns the DOT (Graphviz
  description) of the composite value."
  ([root-value]
   (dot root-value nil))
  ([root-value options]
   {:pre [(satisfies? ds/Composite root-value)]}
   (let [[{:keys [nodes edges]}] (ds/render-composite root-value {})
         options-string-fn #(apply str
                                   (interpose ", "
                                              (map (fn [[k v]] (str (name k) "=" v)) %)))
         font-options (options-string-fn (select-keys options [:fontname]))
         font (if (not= "" font-options) font-options "fontname=\"Hasklug Nerd Font Mono\"")
         root-options (options-string-fn (select-keys options [:bgcolor]))
         node-options (if options
                        (options-string-fn (dissoc options :bgcolor))
                        "shape=plaintext, style=\"rounded,filled\", fillcolor=\"#FAF0E6\"")]
     (with-out-str
       (println "digraph G {\n  rankdir=LR;")
       (println (if (not= "" root-options)
                  (str root-options ";")
                  "bgcolor=\"#FFFFFF00\";"))
       (println (str "graph [" font "];"))
       (println (str "node [" font "];"))
       (println (str "edge [" font "];"))
       (println (str "\n  node [" node-options "];"))

       (doseq [[id text] nodes]
         (println (str "  " id " [" text "];")))

       (println)

       (doseq [[from to] edges]
         (println (str "  " from " -> " to ";")))

       (println "}"))
     )))

(def graphviz-defaults {
                        :shape "plaintext"
                        :style "\"rounded,filled\""
                        ;; :color "\"#FFFFFF\""
                        :fontname "\"Hasklug Nerd Font Mono\""
                        :fontcolor "\"#FFFFFF00\""
                        :fillcolor "\"#555555\""
                        :bgcolor "\"#FFFFFF00\""
                        })

(defn data->svg
  ([data]
   (data->svg data graphviz-defaults))
  ([data options]
   (-> data
       (dot options)
       viz/dot->svg
       print)))
